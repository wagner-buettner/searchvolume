# README #

### Sellics Assignment - Search-Volume API ###

This is the Sellics Assignment - Search-Volume API. 
Its goal is to receive a single keyword as an input and should return a score for that same exact keyword. 

* Version 0.0.1

### Set Up ###
* This project uses [Java 8], [Spring boot](https://spring.io/projects/spring-boot)

# API Documentation
* Swagger-UI is accessible on `http://localhost:8080/swagger-ui.html` for API documentation

### Running the project locally ###
* Run `mvn clean install`, get into the target folder and Run `java -jar searchvolume-0.0.1-SNAPSHOT.jar`
* And then, the application must run properly. It's possible to access it from: http://localhost:8080/swagger-ui.html#/
* For developing, you should install lombok plugin in your IDE

e.g.: http://localhost:8080/estimate?keyword=laptop
