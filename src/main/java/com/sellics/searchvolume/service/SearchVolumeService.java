package com.sellics.searchvolume.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sellics.searchvolume.domain.AmazonRequest;
import com.sellics.searchvolume.domain.AmazonResponse;
import com.sellics.searchvolume.domain.Score;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;

@Service
public class SearchVolumeService {

    private static final String AMAZON_URL_QUERY_BASE = "https://completion.amazon.com/search/complete";
    private static final HttpHeaders HTTP_HEADERS;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    static {
        HTTP_HEADERS = new HttpHeaders();
        HTTP_HEADERS.set(HttpHeaders.CONTENT_TYPE, "text/javascript;charset=UTF-8");
    }

    @Autowired
    public SearchVolumeService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public Score getAmazonResponse(AmazonRequest request) throws IOException {

        String URL = UriComponentsBuilder.fromUriString(AMAZON_URL_QUERY_BASE)
                .queryParam("search-alias", request.getSearchAlias())
                .queryParam("client", request.getClient())
                .queryParam("mkt", request.getMkt())
                .queryParam("q", request.getQuery())
                .build()
                .toUriString();

        Resource resource = restTemplate.exchange(URL, HttpMethod.GET, new HttpEntity<>(HTTP_HEADERS), Resource.class).getBody();

        if (resource != null && resource.exists()) {
            AmazonResponse amazonResponse  = objectMapper.readValue(resource.getInputStream(), AmazonResponse.class);
            Integer score = calculateScore(amazonResponse.getSuggestions(), amazonResponse.getAlias());

            return new Score(amazonResponse.getAlias(), score);
        }
        return null;
    }

    private Integer calculateScore(List<String> suggestionList, String searchKeyword) {
        Integer scoreValue = 0;

        for (int i = 0 ; i < suggestionList.size(); i++) {
            if(suggestionList.get(i).contains(searchKeyword)) {
                scoreValue += 10;
            }
        }
        return scoreValue;
    }
}
