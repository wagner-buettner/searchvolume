package com.sellics.searchvolume.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.sellics.searchvolume.domain.AmazonResponse;
import com.sellics.searchvolume.domain.Structure;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class AmazonResponseDeserializer extends StdDeserializer<AmazonResponse> {

    private static final int IDX_ALIAS = 0;
    private static final int IDX_SUGGESTIONS = 1;
    private static final int IDX_STRUCTURES = 2;
    private static final int IDX_PREFIX = 4;
    private static final int RESPONSE_LENGTH = 5;

    private final ObjectMapper mapper = new ObjectMapper();

    public AmazonResponseDeserializer() {
        this(null);
    }

    private AmazonResponseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public AmazonResponse deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {

        return hasArray(jsonParser.getCodec().readTree(jsonParser)).map(this::unwrap).orElse(null);
    }

    private Optional<ArrayNode> hasArray(JsonNode node) {
        if (isAvailableNode(node)) {
            return Optional.of(((ArrayNode) node));
        }
        return Optional.empty();
    }

    private AmazonResponse unwrap(ArrayNode array) {
        try {
            AmazonResponse resp = new AmazonResponse();

            resp.setAlias(array.get(IDX_ALIAS).asText());
            resp.setSuggestions(unwrapSuggestions(array.get(IDX_SUGGESTIONS)));
            resp.setStructures(unwrapStructures(array.get(IDX_STRUCTURES)));
            resp.setPrefix(array.get(IDX_PREFIX).asText());

            return resp;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<String> unwrapSuggestions(JsonNode node) throws IOException {
        return unwrap(node, new TypeReference<List<String>>() {});
    }

    private List<Structure> unwrapStructures(JsonNode node) throws IOException {
        return unwrap(node, new TypeReference<List<Structure>>() {})
                .stream()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

    private <T> T unwrap(JsonNode node, TypeReference<T> typeReference) throws IOException {
        return this.mapper.readValue(node.toString(), typeReference);
    }

    private boolean isAvailableNode(JsonNode node) {
        return Objects.nonNull(node) && !node.isNull() && node.isArray() && node.size() == RESPONSE_LENGTH;
    }

}
