package com.sellics.searchvolume;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchvolumeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchvolumeApplication.class, args);
	}

}
