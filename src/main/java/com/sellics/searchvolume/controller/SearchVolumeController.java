package com.sellics.searchvolume.controller;

import com.sellics.searchvolume.domain.AmazonRequest;
import com.sellics.searchvolume.domain.Score;
import com.sellics.searchvolume.service.SearchVolumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class SearchVolumeController {

    private final SearchVolumeService searchVolumeService;

    @Autowired
    public SearchVolumeController(SearchVolumeService testService) {
        this.searchVolumeService = testService;
    }

    @GetMapping("/estimate")
    public Score estimate(@RequestParam("keyword") String keyword) throws IOException {

        AmazonRequest request = new AmazonRequest();
        request.setSearchAlias("aps");
        request.setClient("amazon-search-ui");
        request.setMkt(1);
        request.setQuery(keyword);

        return searchVolumeService.getAmazonResponse(request);
    }
}
