package com.sellics.searchvolume.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StructureDetails {

    private String name;
    private String alias;
}
