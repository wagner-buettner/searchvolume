package com.sellics.searchvolume.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmazonRequest {

    private String searchAlias;
    private String client;
    private Integer mkt;
    private String query;
}
