package com.sellics.searchvolume.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AmazonResponse {

    private String alias;
    private List<String> suggestions;
    private List<Structure> structures;
    private String prefix;
    private Integer score;
}
