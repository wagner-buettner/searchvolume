package com.sellics.searchvolume.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Score {

    private String keyword;
    private Integer score;
}
