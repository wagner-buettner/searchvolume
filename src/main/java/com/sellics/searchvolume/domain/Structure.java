package com.sellics.searchvolume.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Structure {

    private List<StructureDetails> nodes;

    @JsonIgnore
    public boolean isEmpty() {
        return nodes == null;
    }
}
